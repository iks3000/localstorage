module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        'airbnb-base',
    ],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module',
    },
    rules: {
        indent: ['error', 4, { ignoredNodes: ['JSXElement *'] }],
        'object-curly-spacing': ['error', 'always'],
        'template-curly-spacing': ['error', 'always'],
    },
};
