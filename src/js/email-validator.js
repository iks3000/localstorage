/* eslint-disable import/prefer-default-export */
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

const validate = () => {
    const buttonForm = document.querySelector('#joinButton');
    const inputEmail = document.querySelector('#inputSubscribe');

    buttonForm.addEventListener('click', () => {
        const emailValue = inputEmail.value;
        const inputEnding = emailValue.substring(emailValue.indexOf('@') + 1);
        VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);
    }, false);
};

export { validate };
