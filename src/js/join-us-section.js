/* eslint-disable max-classes-per-file */
/* eslint-disable consistent-return */
/* eslint-disable class-methods-use-this */

const getTitle = document.querySelector('#joinTitle');
const getButton = document.querySelector('#joinButton');
const joinSection = document.querySelector('#join');
const formDetails = document.querySelector('#formSubscribe');
const inputEmail = document.querySelector('#inputSubscribe');
const unsubscribeBtn = document.querySelector('#unsubscribeButton');
const emailLocalStorage = localStorage.getItem('email');

const displayForm = () => {
    inputEmail.value = '';
    formDetails.style.display = 'flex';
    unsubscribeBtn.style.display = 'none';
};

const displayUnsubscribe = () => {
    formDetails.style.display = 'none';
    unsubscribeBtn.style.display = 'block';
};

const events = () => {
    window.addEventListener('load', () => {
        if (joinSection !== null) joinSection.style.display = 'block';
        if (inputEmail !== null) {
            formDetails.addEventListener('submit', (e) => {
                e.preventDefault();

                localStorage.setItem('email', inputEmail.value);
                displayUnsubscribe();
            }, false);
        }
        unsubscribeBtn.addEventListener('click', () => {
            localStorage.clear();
            displayForm();
        });
        if (emailLocalStorage) {
            displayUnsubscribe();
        } else {
            displayForm();
        }
    });
};

class Creator {
    constructor(type, title, button) {
        this.type = type;
        this.title = title;
        this.button = button;
    }
}

class SectionCreator {
    create(type) {
        if (type === 'standard') return new Creator(type, 'Join Our Program', 'Subscribe');
        if (type === 'advanced') return new Creator(type, 'Join Our Advanced Program', 'Subscribe to Advanced');
    }
}

const obj = new SectionCreator();

function changeContentStandard() {
    window.addEventListener('load', () => {
        getTitle.innerHTML = obj.create('standard').title;
        getButton.innerHTML = obj.create('standard').button;
    });
}

function changeContentAdvance() {
    window.addEventListener('load', () => {
        getTitle.innerHTML = obj.create('advanced').title;
        getButton.innerHTML = obj.create('advanced').button;
    });
}

const remove = () => {
    document.querySelector('#join').remove();
};

export {
    events, changeContentStandard, changeContentAdvance, remove,
};
